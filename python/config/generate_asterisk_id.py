#!/usr/bin/env python

import uuid

print "MYIDENTITY => %s" % hex(uuid.getnode()).lstrip("0x").rstrip("L").zfill(12)