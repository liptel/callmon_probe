#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Config(object):
    ## Communication configuration
    # Zabbix sever IP address
    host                        = '10.0.1.240'
    # Zabbix port to communicate
    port                        = 10051
    # Zabbix server web admin username
    zabbix_login                = 'Admin'
    # Zabbix server web admin password
    zabbix_password             = 'zabbix'
    # Zabbix fronted web interface
    zabbix_url                  = 'http://%s/zabbix'                        % host
    # Username the probe will use to log in to the zabbix server using SSH
    server_ssh_login_username   = 'root'

    ## Probe files and directories configuration
    # Main directory, under which all the scripts are placed
    tree_base                   = '/opt/vqm-sondy'
    # Python base directory
    base_dir                    = '%s/python/'                              % tree_base
    # backup directory for the wav files before they are sent to server
    bckup_wav_dir               = '%s/python/archive/'                      % tree_base
    # directory for the log files
    log_base                    = '%s/python/logs/'                         % tree_base
    # template for sip.conf
    template_sipconf            = '%s/templates/asterisk/sip.conf'          % tree_base
    # generated sip.conf
    sipconf                     = '%s/asterisk/sip.conf'                    % tree_base
    # template for extensions.conf
    template_extconf            = '%s/templates/asterisk/extensions.conf'   % tree_base
    # generated extensions.conf
    extconf                     = '%s/asterisk/extensions.conf'             % tree_base
    # SSH private key
    rsa_key_path                = '%s/ssh/sonda1_rsa'                       % tree_base
    # Reference wav file to be played in calls
    reference_wav               = '%s/wavs/test_ulaw_25s.wav'               % tree_base
    # Asterisk monitor directory
    recorded_dir                = '/var/spool/asterisk/monitor/'
    # Cron user for the task autostart
    probe_cron_user             = 'root'
    # Asterisk AMI interface username
    asterisk_ami_user           = 'python'
    # Asterisk AMI interface password
    asterisk_ami_secret         = 'python'
    # Probe interface used for all the communications
    probe_com_interface_name    = 'eth0'
    # Asterisk configuration for sip channel
    astsipconf                  = '/etc/asterisk/sip.conf'

    ## Server files and directories configuration
    # Directory where the wav files are stored on the server
    server_wav_dir              = '/home/python/data_wavs'


