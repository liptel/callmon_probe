#!/usr/bin/env python
# -*- coding: utf-8 -*-

import itertools as it


class AsteriskCallList(object):
    """
    Creates the list of probes this probe should make connections with. It is the minimal subset of the probes to make
    sure that all the connections will be tested.
    """
    def __init__(self, inp, identity):
        self.inp            = map(str, list(inp))
        self.working_inp    = self.inp[:]
        #self.init_helper_conns2()
        self.identity       = str(identity)
        self.even_conns     = self.init_helper_conns()
        self.acl            = self.init_helper_acl() # This is what is important - returns list of tuples in format of [(origin, destination), ....]

    def init_helper_conns(self):
        n               = len(self.inp)
        no_of_conns     = n * (n - 1) / 2
        conns_per_node  = no_of_conns // n
        rem_conns       = no_of_conns % n
        return [ conns_per_node + 1 if idx < rem_conns else conns_per_node for idx, _ in enumerate(self.inp) ]

    def reorder(self):
        self.working_inp.append(self.working_inp.pop(0))

    def init_helper_acl(self):
        acl = []
        for item in self.even_conns:
            if self.working_inp[0] == self.identity:
                for idx, comb in enumerate(it.combinations(self.working_inp, 2)):
                    if idx < item:
                        acl.append(comb)
                    else:
                        break
            self.reorder()
        return acl

