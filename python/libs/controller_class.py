#!/usr/bin/env python
# -*- coding: utf-8 -*-
from config.config              import Config
from pprint                     import pprint
from libs.zabbix_tcp_class      import ZabbixTcp
from libs.ssh_class             import Ssh
from libs.zabbix_json_class     import ZabbixJson
from libs.crontab_class         import *
from libs.acl_class             import AsteriskCallList
from libs.asterisk_ami_class    import AstConnection
import uuid
import re
import os
import datetime
import netifaces
import itertools
import subprocess
import shutil

## Class for managing and initialization other objects, using their functions and interaction between them.
class Controller(object):
    """
    Main class used to control the functionalities of the probe.
    """
    ## Creating of objects using inside controller
    def __init__(self):
        self.Tcp    = ZabbixTcp()
        self.Jsn    = ZabbixJson()
        self.Cfg    = Config()
        self.Cron   = CronTab(user=self.Cfg.probe_cron_user)
        self.ip     = self.get_ip()
        self.mac    = self.get_mac()

    def run_cmd(self, cmd=None, args=[], outfile=None, errfile=None):
        if cmd:
            popen_cmd = [cmd] + args
            try:
                if outfile and errfile:
                    p = subprocess.Popen(
                            popen_cmd,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE
                            )
                    stdout, stderr = p.communicate()
                    for fout, content in [(outfile,stdout), (errfile,stderr)]:
                        with open(fout, 'a') as f:
                            f.write(content)
                else:
                    p = subprocess.Popen(popen_cmd)
            except OSError as e:
                print "Execution of command %s failed: " % (cmd), e
                print "Checking whether the command was run with sudo."
                if cmd == 'sudo':
                    print "Command executed with sudo and failed, therefore stripping."
                    self.run_cmd(cmd=args[0], args=args[1:], outfile=outfile, errfile=errfile)
                else:
                    print "Commamd run without sudo, continuing."
        else:
            print "Command not specified."
        return

    ## add task to cron table
    def add_cron(self, command, period):
        job  = self.Cron.new(command)
        job.minute().every(period)
        self.Cron.write()

    ## add task to cron table for same time in every hour
    def add_cron_in_hour(self, command, period):
        job  = self.Cron.new(command)
        job.minute().on(int(period))
        self.Cron.write()

    ## remove task from cron table
    def remove_cron(self, command):
        finded = self.find_cron(command)
        for item in finded:
            self.Cron.lines.remove(item)
        self.Cron.write()

    ## remove all cron tasks from cron table
    def remove_all_crons(self):
        #self.Cron       = CronTab(user=self.Cfg.probe_cron_user)
        self.Cron.lines = []
        self.Cron.write()

    ## display actual cron table with all tasks
    def print_cron(self):
        print self.Cron.render()

    ## find task by substring that contains
    def find_cron(self, command):
        return self.Cron.find_command(command)

    ## get own mac address of probe
    def get_mac(self):
        return str(hex(uuid.getnode()).lstrip("0x").rstrip("L")).zfill(12)

    ## get own ip address of probe
    def get_ip(self):
        return netifaces.ifaddresses(self.Cfg.probe_com_interface_name)[netifaces.AF_INET][0]['addr']

    ## check if exist probe with this mac address on zabbix server
    def probe_exist(self):
        return self.Jsn.check_host(self.mac);

    ## create new probe on zabbix server with ip and mac of this computer
    def create_probe(self):
        params = {
            "host": self.mac,
            "name": self.ip,
            "groups":[
            {
               "groupid":4
            }
            ],
            "templates":[
                {
                    "templateid":10105
                }
            ],
            "interfaces":[
                {
                    "type":1,
                    "main":1,
                    "useip":1,
                    "ip":"127.0.0.1",
                    "dns":"",
                    "port":10050
                }
            ]
        }
        return self.Jsn.add_host(params)

    ## set status of probe to active
    def activate_probe(self):
        return self.Jsn.activate_host(self.ip)

    ## generating new sip.conf file with all neighbours of probe
    def generate_conf(self):
        hosts = self.Jsn.get_hosts(['host','name'])
        items = ''
        try:
            with open(self.Cfg.template_sipconf, 'r') as f:
                content = f.read()
        except IOError as e:
            print e
            return False
        matches = re.findall( r'##FOREACH(.*?)FOREACH##', content, re.DOTALL) #(.*)FOREACH##
        for fc in matches:
            for host in hosts:
                if host['name'] != self.ip:
                    subcontent = fc.replace('##ip##',host['name'])
                    subcontent = subcontent.replace('##mac##',host['host']).replace('##my_mac##',self.mac)
                    items += subcontent

        content = content.replace(fc, items)
        content = content.replace("##FOREACH","")
        content = content.replace("FOREACH##","")
        try:
            with open(self.Cfg.sipconf, 'w') as f:
                f.write(content)
        except IOError as e:
            print e
            return False
        shutil.copy(self.Cfg.sipconf, self.Cfg.astsipconf)
        self.run_cmd('sudo', args=['asterisk', '-rx', 'core restart now'])
        return True


    ## load new files from probe to the server and than their backup to different directory
    def rsync(self):
        fileList = os.listdir(self.Cfg.recorded_dir)
        self.run_cmd(
                'rsync',
                args=['-a', self.Cfg.recorded_dir, self.Cfg.bckup_wav_dir],
                outfile='/dev/null'
                #errfile='/dev/null'
                )
        self.run_cmd(
                'rsync',
                args=[
                    '-av',
                    '--remove-source-files',
                    '--rsh=ssh -i %s' % self.Cfg.rsa_key_path,
                    self.Cfg.recorded_dir,
                    'root@%s:%s' % (self.Cfg.host, self.Cfg.server_wav_dir)
                    ],
                outfile='/dev/null'
                )
        return fileList

    ## generating new calls between half of known probes from topology
    def send_calls(self):
        ret                 = {'call_number':0, 'ok':0, 'error':0}
        list_of_hosts       = sorted([host['host'] for host in self.Jsn.get_hosts(['host'])])
        acl                 = AsteriskCallList(list_of_hosts, self.mac)
        local_acl           = acl.acl     # list of tuples [(origin, destination), ....]
        asc                 = AstConnection(self.Cfg)

        f = open(self.Cfg.log_base + 'call.log', "w")

        for item in local_acl:
            f.write( str(datetime.datetime.now())+' - Start call from %s to: Local/%s@local-loop \n' % (self.mac, item[1]))
            asc.connect()
            asc.send_action('login')
            if asc.read_response('Success'):
                asc.send_action(
                                action      = 'Originate',
                                attributes  = {
                                                'Channel' : 'Local/%s@local-loop' % item[1],
                                                'Context' : 'to-probe',
                                                'Exten'   : '%s' % item[1],
                                                'Priority': '1',
                                                'Timeout' : '30000'
                                                }
                                )
                print "Calling to:"+item[1]
                ret['call_number'] += 1
                if asc.read_response('Success'):
                    ret['ok'] += 1
                else:
                    ret['error'] += 1
            else:
                print "Can't connect to Asterisk"
                return False
        f.close()
        print "Statistics: "+str(ret)
        return ret

    ## restarting of asterisk service
    def asterisk_restart(self):
        self.run_cmd('/etc/init.d/asterisk', args=['restart'])

    ## runing analyzer script on server side for getting quality of call
    def run_analyzation(self, f):
        target_mac = self.get_target_mac(f)
        item = 'pesq-'+ target_mac
        item_exist = self.Jsn.check_item(item, self.mac) # if not exist item on this probe
        if item_exist != True and self.mac != target_mac:
            print "Add new item '"+item+"' on host "+self.ip
            self.Jsn.add_item(item, self.mac)   # Create new item

        self.Ssh = Ssh( self.Cfg.host, 'ssh')
        if self.Ssh.command(
                'callmons add_task ' +
                ' ' +
                f +
                ' ' +
                self.mac
                ) != False :
            print "File added to processing queue on zabbix server"
            return True
        else:
            print "Error:Cannot add file to processing queue on server"
            return False

    ## parse name of recorded file and get target probe mac address
    def get_target_mac(self, file):
        parts = file.split('-')
        try:
            ret = parts[1]
        except IndexError:
            ret = False
        return ret

    ## create items from every probe to all anothers (all connections)
    def create_edges(self):
        hosts = self.Jsn.get_hosts(['host'])
        for item in hosts:
            for i in hosts:
                if item['host'] != i['host']:
                    print "create:"+item['host']+" "+i['host']
                    item_exist = self.Jsn.check_item('pesq-'+item['host'], i['host']) #IF not exist item on this probe
                    if item_exist != True:
                        self.Jsn.add_item( 'pesq-'+item['host'],i['host'])

