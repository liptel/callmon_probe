#!/usr/bin/env python
# -*- coding: utf-8 -*-


folder_asterisk_config  = '/etc/asterisk'
folder_asterisk_monitor = '/var/spool/asterisk/monitor'
PROBE_USER      = False
TREEBASE        = None
ASTBASE         = None
ZABBIXSSHUSER   = None
ZABBIXIP        = None
RSAKEY          = None
PLATFORM        = None

required_modules = (
        'datetime',
        'itertools',
        'json',
        'logging',
        'netifaces',
        'os',
        'paramiko',
        'pprint',
        'pwd',
        'random',
        're',
        'requests',
        'shutil',
        'socket',
        'stat',
        'struct',
        'subprocess',
        'sys',
        'telnetlib',
        'tempfile',
        'time',
        'traceback',
        'uuid',
        'pickle',
        )



def check_modules():
    print "Checking required modules:"
    for module in required_modules:
        try:
            __import__(module)
            print "{:<20}{:>20}".format('Module %s:' % module, '[OK]')
        except:
            print "{:<20}{:>20}".format('Module %s:' % module, '[FAIL]')
            print "Cannot import module %s, please install it and run this script again." % module
            return False
    return True

def check_platform():
    global PLATFORM
    print "Checking the platform where the probe is run."
    if os.path.isfile('/etc/openwrt_release'):
        PLATFORM = 'OWRT'
    elif os.path.isfile('/etc/issue'):
        PLATFORM = 'LINUX'
    else:
        print "Unknown platform, exiting."
        sys.exit()
    return

def check_ssh_client():
    global SSHCLIENT
    client = user_input(
        question        = 'What type of SSH client is used on the probe? [OpenSSH] ',
        expected_answer = '^(openssh|dropbear)$',
        default_answer  = 'openssh',
        flag            = re.IGNORECASE
    ).lower()
    if client:
        SSHCLIENT = client
    else:
        print "Unknown SSH client defined, exiting. Please use one of {OpenSSH, Dropbear}"
        sys.exit()
    return


def check_rsa():
    global RSAKEY
    if SSHCLIENT == 'openssh':
        print "Checking if there is an OpenSSH private key present in ./ssh directory."
    else:
        print "Checking if there is an OpenSSH private key present in ./ssh directory."
    ssh_dir   = os.path.join(cwd, 'ssh')
    key_found = False
    for f in os.listdir(ssh_dir):           # list all files in ssh directory
        possible_key = os.path.join(ssh_dir, f)
        if os.path.isfile(possible_key):
            with open(possible_key, 'r') as open_file:
                first_line = open_file.readline()
            if (
                (
                    SSHCLIENT == 'openssh' and
                    'PRIVATE KEY' in first_line
                ) or (
                    SSHCLIENT == 'dropbear' and
                    'ssh-rsa' in first_line
                )
            ):
                RSAKEY = possible_key
                permissions = oct(os.stat(possible_key).st_mode & 0777)
                print "Possible SSH private key found: %s" % possible_key
                print "Checking file permissions: %s" % permissions
                if permissions == '0600':
                    key_found = True
                    print "Permissions correct. Continuing."
                else:
                    confirmation = user_input(
                            question        = 'Do you want to change the file permissions to 0600? [y/N] ',
                            expected_answer = '^(y|n)$',
                            default_answer  = 'n',
                            flag            = re.IGNORECASE
                            )
                    if confirmation == 'y':
                        key_found = True
                        print "Setting correct file permissions for the file %s" % possible_key
                        os.chmod(possible_key, 384)
                    else:
                        print "Skipping setting file permissions for possible SSH private key."
    if not key_found:
        print "No SSH key found or file permissions incorrect. Did you download the SSH private key?"
        sys.exit()
    return

def check_user():
    global PROBE_USER
    user = None
    print 'Checking if the script is running as the probe user.'
    if not PROBE_USER:
        PROBE_USER = user_input(
                question        = 'Please enter the probe user? [%s] ' % pwd.getpwuid(os.getuid()).pw_name,
                expected_answer = '^(%s)$' % '|'.join(map(lambda x: x.pw_name, pwd.getpwall())), # | separated list of all users of the system
                default_answer  = str(pwd.getpwuid(os.getuid()).pw_name)
                )
        print PROBE_USER
    return


def check_folder_permissions(folder):
    outfolder = folder
    if PROBE_USER:
        print 'Attempting to write into the %s directory.' % folder
        try:
            filepath = os.path.join(folder, 'test')
            with open(filepath, 'w') as f:
                f.write('dummy')
            os.remove(filepath)
            print 'Directory %s fully accessible, continuing.' % folder
        except Exception as e:
            if os.path.isdir(folder):
                print 'Error accessing the %s directory. Please check permissions.' % folder
                sys.exit()
            else:
                new_folder = user_input(
                    question = 'Folder %s does not exist. Please enter the replacement folder. ' % folder,
                    expected_answer = '^(/.+)$'
                )
                print "Checking given folder."
                check_folder_permissions(new_folder)
                outfolder = new_folder
    else:
        print 'Please run this script as the same user as the probe will be run by.'
        sys.exit()
    return outfolder

def check_asterisk_permissions():
    if PROBE_USER:
        print "Checking, whether this user can restart Asterisk."
        try:
            p = subprocess.call(['/etc/init.d/asterisk', 'restart'])
            print "Asterisk successfully restarted, continuing."
        except Exception as e:
            print "Cannot restart Asterisk PBX, please check permissions and Asterisk installation."
            print e
            sys.exit()
    else:
        print "Not a probe user please run this script as the same user as the probe will be run by."
        sys.exit()
    return

def create_config():
    global TREEBASE, ASTBASE, ZABBIXSSHUSER, ZABBIXIP
    cfg               = 'templates/python/config.py'
    astmon            = folder_asterisk_monitor + '/'
    sipconf           = folder_asterisk_config + '/sip.conf'
    cron_user         = PROBE_USER
    ami_user          = 'python'
    ami_pass          = 'python'
    probe_interface   = 'eth0'
    zabbixport        = '10051'
    print 'Preparing the configuration of the probe, please answer the following questions.'
    zabbix_ip         = user_input(
            question        = 'What is the IP address of the zabbix server? ',
            expected_answer = '^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$',
            default_answer  = None
            )
    zabbix_port       = user_input(
            question        = 'What is the port of the zabbix server? [%s]' % zabbixport,
            expected_answer = '^(\d{1,4}|[1-5]\d{4}|6[0-4]\d{3}|65[0-4]\d{2}|655[0-2]\d{1}|6553[0-5])$',
            default_answer  = zabbixport
            )
    zabbix_login      = user_input(
            question        = 'Please enter the username to be used for logging in the Zabbix server. ',
            expected_answer = '.*',
            default_answer  = None
            )
    zabbix_pass       = user_input(
            question        = 'Please enter the password to be used for logging in the Zabbix server. ',
            expected_answer = '.*',
            default_answer  = None
            )
    zabbix_ssh_user   = user_input(
            question        = 'Please enter the ssh user for logging in the Zabbix server. ',
            expected_answer = '.*',
            default_answer  = None
            )
    treebase          = user_input(
            question        = 'Please enter the root directory (absolute path) for the probe scripts. [%s] ' % cwd,
            expected_answer = '^(\/.*?)+$',
            default_answer  = cwd
            )
    record_dir        = user_input(
            question        = 'Please enter the directory where the asterisk stores the monitoring files. [%s] ' % astmon,
            expected_answer = '^(\/.*?)+$',
            default_answer  = astmon
            )
    probe_cron_user   = user_input(
            question        = 'Please enter the user for the probe crontab entries. [%s] ' % cron_user,
            expected_answer = '^(%s)$' % '|'.join(map(lambda x: x.pw_name, pwd.getpwall())),
            default_answer  = cron_user
            )
    asterisk_ami_user = user_input(
            question        = 'Please enter the user for the Asterisk AMI Interface. [%s] ' % ami_user,
            expected_answer = '^(%s)$' % '|'.join(map(lambda x: x.pw_name, pwd.getpwall())),
            default_answer  = ami_user
            )
    asterisk_ami_pass = user_input(
            question        = 'Please enter the password for the Asterisk AMI Interface. [%s] ' % ami_pass,
            expected_answer = '.*',
            default_answer  = ami_pass
            )
    interface         = user_input(
            question        = 'Please enter the probe interface for the network communication. [%s] ' % probe_interface,
            expected_answer = '.*',
            default_answer  = probe_interface
            )
    asterisk_sipconf  = user_input(
            question        = 'Please enter the location of the asterisk sip.conf. [%s] ' % sipconf,
            expected_answer = '^(\/.*?)+sip\.conf$',
            default_answer  = sipconf
            )

    relation_dictionary = {
            'ZABBIXIP'            : zabbix_ip,
            'ZABBIXPORT'          : zabbix_port         if zabbix_port       != '' else zabbixport,
            'ZABBIXLOGIN'         : zabbix_login,
            'ZABBIXPASSWORD'      : zabbix_pass,
            'ZABBIXSSHUSER'       : zabbix_ssh_user,
            'PROBEBASEDIR'        : treebase            if treebase          != '' else cwd,
            'ASTMONDIR'           : record_dir          if record_dir        != '' else astmon,
            'PROBECRONUSER'       : probe_cron_user     if probe_cron_user   != '' else cron_user,
            'PROBEASTAMIUSER'     : asterisk_ami_user   if asterisk_ami_user != '' else ami_user,
            'PROBEASTAMIPASSWORD' : asterisk_ami_pass   if asterisk_ami_pass != '' else ami_pass,
            'PROBEINTERFACE'      : interface           if interface         != '' else probe_interface,
            'PROBEASTSIPCONF'     : asterisk_sipconf    if asterisk_sipconf  != '' else sipconf,
            }
    TREEBASE        = relation_dictionary['PROBEBASEDIR']
    ASTBASE         = relation_dictionary['PROBEASTSIPCONF'].rstrip('/sip.conf')
    ZABBIXSSHUSER   = relation_dictionary['ZABBIXSSHUSER']
    ZABBIXIP        = relation_dictionary['ZABBIXIP']

    with open(cfg, 'r') as f:
        content = f.read()

    for key, val in relation_dictionary.iteritems():
        content = content.replace(key, val)

    with open('python/config/config.py', 'w') as f:
        f.write(content)
    print "Config generation completed successfully."
    return

def prepare_asterisk():
    configs_to_copy = [
            'asterisk.conf',
            'manager.conf',
            'generate_asterisk_id.py'
            ]
    if not TREEBASE or not ASTBASE:
        print 'Error. Paths not set, exitting.'
        sys.exit()
    print "Configuring Asterisk extensions.conf."
    with open('templates/asterisk/extensions.conf', 'r') as f:
        content = f.read()
    content = content.replace('##FILETOPLAY##', TREEBASE + '/wavs/' +'test_ulaw_25s')

    with open(ASTBASE + '/extensions.conf', 'w') as f:
        f.write(content)
    print 'Extensions.conf successfully configured, continuing.'
    print 'Copying necessary preconfigured files.'

    for f in configs_to_copy:
        try:
            print 'Trying to copy %s...' % f
            shutil.copy('asterisk/' + f, ASTBASE )
        except Exception as e:
            print 'Error while copying the file %s.' % f
            print e
            print 'Please check the error and run this script again.'
            sys.exit()

    try:
        p = subprocess.call(['/etc/init.d/asterisk', 'restart'])
    except Exception as e:
        print 'Error occured while restarting Asterisk.'
        print e
    return

def connect_to_server():
    if not RSAKEY or not ZABBIXSSHUSER or not ZABBIXIP:
        print 'Connection information not set, exiting.'
        sys.exit()
    print 'Establishing initial SSH connection to the server'
    try:
        status = subprocess.call(['ssh', '-i', RSAKEY, '-T',ZABBIXSSHUSER + '@' + ZABBIXIP, 'exit'])
        if status != 0:
            print "Error while trying to connect to the zabbix server."
            sys.exit()
        else:
            print 'Connection successfully established'
    except Exception as e:
        print 'Unable to connect to the Zabbix server using SSH.'
        print e
        print 'Please run this script again.'
        sys.exit()
    return

def system_configuration():
    probe_user_home = os.path.expanduser('~')
    fprofile = user_input(
            question        = 'Where are the environmental variables for non-interactive shell defined? [%s/.bashrc] ' % probe_user_home,
            expected_answer = '^(\/.*?)+$',
            default_answer  = '%s/.bashrc' % probe_user_home
            )
    print 'Trying to insert callmon system variables.'
    try:
        # try to lookup non-interactive shell clause

        with open(fprofile, 'r') as f:
            content = f.read()
        noninteractive_clause = '[ -z "$PS1" ]'
        if noninteractive_clause in content:
            if TREEBASE + '/python' not in content:
                content = content.replace(noninteractive_clause, 'export PATH=$PATH:%s\n%s' % (TREEBASE+'/python', noninteractive_clause))
            else:
                print "Environment already set."
        else:
            # if there is no noninteractive clause in the file, search for the export PATH= clause
            if 'export PATH=' in content:
                if TREEBASE + '/python' not in content:
                    current_path = re.search('export PATH=(.+?)\n', content).group(1)
                    content = content.replace(current_path, '%s:%s' % (current_path, TREEBASE + '/python'))
                else:
                    print "Environment already set."
            else:
                content = content + '\n' + 'export PATH=' + TREEBASE + '/python'
        with open(fprofile, 'w') as f:
            #f.write('export CALLMONP_PATH=%s\n' % TREEBASE)
            #print 'Probe path successfully introduced.'
            print 'Modifying the system path to include probe variables.'
            f.write(content)
            #f.write('export PATH=$PATH:%s\n' % (TREEBASE + '/python'))
            #print 'System path successfully updated.'
    except Exception as e:
        print 'Error while trying to modify %s' % fprofile
        print e
        print 'Please enter the environmental variables manually.'
        print 'export CALLMONP_PATH=%s' % TREEBASE
        print 'export PATH=$PATH:%s' % (TREEBASE + '/python')
        sys.exit()

    return

def set_permissions():
    print 'Setting callmonp.py to be executable.'
    try:
        os.chmod(TREEBASE + '/python/callmonp', 511)
    except Exception as e:
        print e
    return

def user_input(question=None, expected_answer=None, default_answer=None, flag=None):
    if question:
        var = raw_input(question)
        if flag:
            condition_check = re.match(expected_answer, var, flag)
        else:
            condition_check = re.match(expected_answer, var)
        if condition_check:
            return str(var)
        else:
            if default_answer:
                print 'WARNING: Invalid entry given, using default [%s].' % default_answer
                return str(default_answer)
            else:
                print 'No permitted input given, please try again'
                user_input(question=question, expected_answer=expected_answer, default_answer=default_answer, flag=flag)


if __name__ == "__main__":
    import sys
    if not check_modules():
        sys.exit()
    else:
        import os
        import stat
        import subprocess
        import shutil
        import sys
        import pwd
        import re
        cwd = os.getcwd()
    check_platform()
    check_ssh_client()
    check_rsa()
    check_user()
    check_asterisk_permissions()
    folder_asterisk_config  = check_folder_permissions(folder_asterisk_config)
    folder_asterisk_monitor = check_folder_permissions(folder_asterisk_monitor)
    create_config()
    prepare_asterisk()
    connect_to_server()
    system_configuration()
    set_permissions()

    setup = raw_input('Do you want to activate the probe now? [Y/n] ')
    if setup == '' or setup == 'y' or setup == 'Y':
        print "Calling the probe script logic."
        try:
            subprocess.call(['python/callmonp', 'main'])
            subprocess.call(['python/callmonp', 'create_edges'])
            subprocess.call(['python/callmonp', 'refresh_conf'])
            print "Callmon logic successfully executed and probe set up."
            print "IMPORTANT: To allow the system to use callmonp command, please restart the session."
            print "Thank you for using CALLMON probe by CESNET, z.s.p.o."
        except Exception as e:
            print e
    else:
        print "Skipping the probe activation."





